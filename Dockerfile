FROM ubuntu
RUN apt-get update -y && \
    apt-get install -y python3-pip \
    python-dev-is-python3

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
ENTRYPOINT [ "python" ]
CMD [ "igt_app.py" ]